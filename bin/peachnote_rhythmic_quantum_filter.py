# -*- coding: utf-8 -*-
"""
Created on Sat May 10 17:53:15 2014

@author: Eidan
"""

import sys,os
sys.path.append(os.path.abspath('../'))
from bach.peachnote import *
import argparse

def filter_by_rhythm(rhythm_quantum, instream, outstream):
    dfilter = create_quantum_dfilter(rhythm_quantum)
    i = 0
    for pdg in datagram_iterator(instream, dfilter):
        if i % 10000 == 0:
            print 'progress',i
        i += 1
        outstream.write(str(pdg) + '\n')
        
def filter_file_by_rhythm(rhythm_quantum, infile, outfile):
    with open(infile, 'r') as infp:
        with open(outfile, 'w') as outfp:
            filter_by_rhythm(rhythm_quantum, infp, outfp)

def main(rhythm_quantum, outstream, infiles):
    if infiles is None:
        filter_by_rhythm(rhythm_quantum, sys.stdin, outstream)
    else:
        for infile in infiles:
            with open(infile, 'r') as infp:
                filter_by_rhythm(rhythm_quantum, infp, outstream)
    return


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Filter out non-conforiming rhythmic values from a peachnote datagram file')
    parser.add_argument('quantum', help='The rhythmic quantum', type=int)
    parser.add_argument('-o', '--outfile' , help='The name of the output file')
    parser.add_argument('infiles', nargs='*', help='The names of the input files')
    args = parser.parse_args()
    if args.outfile is None:
        main(args.quantum, sys.stdout, args.infiles)
    else:
        with open(outfile, 'w') as outfp
            main(args.quantum, outfp, args.infiles)
        