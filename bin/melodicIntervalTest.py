# -*- coding: utf-8 -*-
"""
melodicIntervalTest.py

Some code to test out feature extraction and learning using SVM

Created on Fri Apr 11 17:19:29 2014

@author: Eidan
"""

from music21.features.jSymbolic import MelodicIntervalHistogramFeature
from music21 import corpus

from sklearn import svm

def extractFeature(path):
    """
    Get a path of a file in the corpus and return the feature vector of the 
    MelodicIntervalHistogramFeature feature.
    """
    s = corpus.parse(path)
    fe = MelodicIntervalHistogramFeature(s)
    f = fe.extract()
    return f.vector

def test(N = 4):
    """
    Run SVM learning on N madrigals and N chorales using the 
    MelodicIntervalHistogramFeature feature.
    """
    choralePaths = corpus.CoreCorpus().getBachChorales()[:N]
    choraleVectors = list(map(extractFeature, choralePaths))
    
    madrigalPaths = corpus.getComposer('monteverdi')[:N]
    madrigalVectors = list(map(extractFeature, madrigalPaths))
    
    X = madrigalVectors + choraleVectors
    y = N*['madrigal'] + N*['chorale']
    
    clf = svm.SVC()
    clf.fit(X,y)
    
    return clf
