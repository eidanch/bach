# -*- coding: utf-8 -*-
"""
Created on Thu Jun 05 17:39:35 2014

@author: Eidan
"""

def readIntervalDictionary(filename):
    result = dict()
    with open(filename, 'r') as fp:
        for line in fp:
            lst = line.strip().split('\t')
            interval, year, count = tuple(map(int,lst))
            if interval not in result:
                result[interval] = count
            else:
                result[interval] += count
    return result
    
def writeIntervalDictionary(dictionary, filename):
    total = 1.*sum(dictionary[k] for k in dictionary)
    with open(filename, 'w') as fp:
        for k in dictionary:
            fp.write("%d %.10f\n" % (k, dictionary[k]/total))
    
if __name__ == '__main__':
    d = readIntervalDictionary("..\\data\\imslp-interval-1gram-20110401.csv")
    writeIntervalDictionary(d, "..\\data\\interval-counts.csv")