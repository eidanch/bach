# -*- coding: utf-8 -*-
"""
Created on Thu Jul 24 16:46:02 2014

@author: Hershenson
"""

from sklearn.svm import OneClassSVM
from numpy import array, zeros
from theory import phraseHarmonicTensionVector

samples = zeros((len(phrases), 32))
for i in range(len(phrases)):
    samples[i] = array(phraseHarmonicTensionVector(phrases[i]))
    
clf = OneClassSVM(nu=0.2)
clf.fit(samples)
print 0.5*mean(1 +clf.predict(samples))