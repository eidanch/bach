# -*- coding: utf-8 -*-
"""
Created on Sun Apr 13 11:50:24 2014

@author: Eidan
"""

import doctest
from music21 import converter, note, interval

def signOfInterval(i):
    """
    Return the sign of the interval's direction: 1 for an ascending interval
    -1 for a descending interval and 0 for a unison.
    
    >>> signOfInterval(interval.Interval('M6'))
    1
    >>> signOfInterval(interval.Interval('P-4'))
    -1
    >>> signOfInterval(interval.Interval('P1'))
    0
    """
    c = i.cents
    if c > 0:
        return 1
    elif c < 0:
        return -1
    else:
        return 0

def contour(s):
    """
    Returns an array of directions of the melodic intervals of a stream
    
    >>> contour(converter.parse("tinynotation: 3/4 c4 d8 f f f g16 a g f#"))
    [1, 1, 0, 0, 1, 1, -1, -1]
    """
    return [signOfInterval(i) for i in s.melodicIntervals()]
    ###
    sf = s.flat.getElementsByClass(note.Note)
    result = list()
    for i in range(len(sf)-1):
        result.append(signOfInterval( \
            interval.notesToChromatic(sf[i], sf[i+1])))
    return result

def durations(s):
    """
    Returns an array of durations for the notes in a stream s
    
    >>> durations(converter.parse("tinynotation: 3/4 c4 d8 f g16 a g f#"))
    [1.0, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25]
    """
    return [n.duration.quarterLength \
        for n in s.flat.getElementsByClass(note.Note)]

def compareNgrams(s1,s2):
    """
    Returns True if and only if the contours of the two Ngrams are identical
    Pending changes.
    """
    return contour(s1) == contour(s2) and durations(s1) == durations(s2)    
        
def contourNgrams(s, N, R):
    """
    Returns a dictionary of Ngram indieces to recurrence count.
    
    s - Measured input stream (first non measrues are to be removed)
    N - Length of Ngram in measures
    R - search range in measures
    """
    sf = s
    result = dict()
    for i in range(len(sf) - N):
        ngram = sf[i:i+N]
        print i, ngram
        for j in range(i+N, min(len(sf) - N, i + N + R)):
            if compareNgrams(ngram,sf[j:j+N]):
                print i,j
                if i not in result:
                    result[i] = 1
                else:
                    result[i] += 1
    return result
        
if __name__ == '__main__':
    doctest.testmod()