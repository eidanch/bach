# -*- coding: utf-8 -*-
"""
Created on Wed Jun 18 12:23:31 2014

@author: Eidan
"""

from music21 import *

def harmonicNoteConformance(nt, chord, scl=None):
    """
    Returns a value representing the relation of a single note to a given chord
    """
    if nt.pitchClass == chord.root().pitchClass:
        return 3
    if nt.pitchClass == chord.third.pitchClass:
        return 2
    if nt.pitchClass == chord.fifth.pitchClass:
        return 1
    if chord.seventh is not None and nt.pitchClass == chord.seventh.pitchClass:
        return 2
    if scl is not None and scl.getScaleDegreeFromPitch(nt.pitch) is None:
        return -100
    return -1

def harmonicConformance(st, chord, scl=None):
    """
    Returns a total conformance metric of a stream to a given chord and scale
    """
    result = 0.0
    cover = [0]*4
    for nt in st.notes:
        hnc = harmonicNoteConformance(nt, chord, scl)
        if hnc > 0:
            cover[hnc] = 1
        result += 2*nt.duration.quarterLength*nt.beatStrength*hnc
    return result*sum(cover)

def isStreamOnScale(st, scl):
    """
    Returns true if the stream completely conforms with the scale
    """
    for nt in st.notes:
        if scl.getScaleDegreeFromPitch(nt.pitch) is None:
            return False
    return True

#numerical estimations of the tension level of each possible chord
harmonicTension = {
    '-I' : 2.5,
    'I' : 0.1,
    '#i' : 2.5,
    '#I' : 7.0,
    '-i' : 3.0, 
    'i' : 0.5,    
    '-ii' : 2.5,    
    'ii' : 4.0,    
    '#ii' : 3.5, 
    '-II' : 7.0,  
    'II' : 5.0,  
    '#II' : 5.0, 
    '-iii' : 3.5,
    'iii' : 2.0,
    'III' : 8.0,
    '-III' : 5.0,
    '-vi' : 1.0,
    'vi' : 2.0,
    '#vi' : 3.0,
    'VI' : 3.5,
    '#VI' : 4.5,
    '-iv' : 2.0,
    '-IV' : 8.0,
    'IV' : 4.0,
    '#IV' : 1.5,
    'iv' : 4.5,
    '-V' : 5.5,
    'V' : 8.0,
    '#v' : 1.0,
    '#V' : 7.0,
    'v' : 3.5,
    '-VI': 7.0,
    '--vii': 2.0,
    '-vii': 3.0,
    '-VII': 6.5,
    'VII': 2.5,
    'vii' : 8.0
}
    


def phraseHarmonicTensionVector(phrase):
    """
    tranforms a harmonic phrase to a vector represantion of tension level on each beat
    """
    beats = int(sum(c.quarterLength for c in phrase))
    result = []
    cind = 0
    charm = phrase[cind]
    cbeats = 0
    for i in range(beats):
        result.append(harmonicTension[charm.romanNumeral])
        cbeats += 1
        if cbeats == charm.quarterLength and cind < len(phrase) - 1:
            cind += 1
            charm = phrase[cind]
            cbeats = 0
    return result        