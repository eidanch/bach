# -*- coding: utf-8 -*-
"""
Created on Tue Jun 24 20:10:06 2014

@author: Eidan
"""

from music21 import *
from peachnote import *
from theory import *
from measurelearner import *
from randomutils import argrand
import random

   
class BachMeasureGenerator(object):
    """
    Represents a measure generator that uses the bach measure corpus
    
    The generator randomly picks a measure from the generator constrained
    and weighted by various conformance metrics
    """
    def __init__(self, voice):
        self.corp = createMeasureCorpus('..\\data\\bach_measures_%s_withts.csv' % voice, '4/4')

    def generate_cocnrete(self ,scale=None, previous=None, harmony=None, rhythm=None, end_of_piece=False):
        if end_of_piece:
            st = stream.Stream()
            nt = note.Note(harmony.root(), quarterLength=4)
            st.append(nt)
            return st
        biasedcorp = getBiasedWeightCorpus(random.sample(self.corp, 80),
                                           scale, harmony)
        ngram = argrand(lambda x: biasedcorp[x],biasedcorp.keys())
        return ngram_to_melodic_stream(ngram)
        
def generateMelodicStream(scl, harmony, voice, end_of_piece=False):
    """
    Generates a melodic stream over a given scale and harmony and for a specific
    voice (Soprano, Alto, Bass )using the BachMeasureGenerator
    """
    mg = BachMeasureGenerator(voice)
    result = stream.Stream()
    ps = None
    for i, c in enumerate(harmony):
        ns = mg.generate_cocnrete(scl, ps, c, None, end_of_piece and i == len(harmony) - 1)
        for n in ns.notes:
            result.append(n)
        ps = ns
    return result
    
def generateAccompenimentStream(harmony):
    """
    Generates a simple Accompeniment stream over a given harmony
    """
    result = stream.Stream()
    for c in harmony:
        ct = c.closedPosition().closedPosition(3)
        result.append(ct)
    return result
    
def testGenerateStream():
    scl = scale.MajorScale('E-')  
    c1 = chord.Chord(['e-','g','b-'], quarterLength=4)
    c2 = chord.Chord(['c','e-','g'], quarterLength=4)
    c3 = chord.Chord(['a-','c','e-'], quarterLength=4)
    c4 = chord.Chord(['b-','d3','f3'], quarterLength=4)
    c5 = chord.Chord(['e-','g','b-'], quarterLength=4)
    c6 = chord.Chord(['f','a-3','c3'], quarterLength=4)
    c7 = chord.Chord(['b-3','d3','f3'], quarterLength=4)
    c8 = chord.Chord(['e-','g3','b-3'], quarterLength=4)
    cs = [c1,c2,c3,c4,c5,c6,c7,c8]
    r = generateMelodicStream(scl, cs, 'Alto') \
        + generateAccompenimentStream(cs)
    r.show()
    
def testGenerateStream2():
    scl = scale.MinorScale('A-')  
    c1 = chord.Chord(['a','c','e'], quarterLength=4)
    c2 = chord.Chord(['d','f','a'], quarterLength=4)
    c3 = chord.Chord(['c','a','e-'], quarterLength=4)
    c4 = chord.Chord(['e','g#','b'], quarterLength=4)
    c5 = chord.Chord(['a','c','e'], quarterLength=4)
    c6 = chord.Chord(['f','a','c'], quarterLength=4)
    c7 = chord.Chord(['e','g#','b'], quarterLength=4)
    c8 = chord.Chord(['a','c','e'], quarterLength=4)
    cs = [c1,c2,c3,c4,c5,c6,c7,c8]
    r = generateMelodicStream(scl, cs, 'Soprano') \
        + generateAccompenimentStream(cs) 
    r.show()

if __name__ == '__main__':
    testGenerateStream2()