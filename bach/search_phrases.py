# -*- coding: utf-8 -*-
"""
Created on Tue Jul 22 17:38:20 2014

@author: Hershenson
"""
import random
import pickle
from music21 import roman, key
from theory import phraseHarmonicDistance, phraseHarmonicTensionVector
from numpy import mean, array, linalg, shape
from bachmeasuregenerator import generateAccompenimentStream

diantonicNumerals = [
    'I',
    'ii',
    'iii',
    'IV',
    'V',
    'vi',
]

def getBreakPoint(phrase, measureInd, keySignature):
    total = 0
    index = 0
    print 'index:',measureInd, 'sig:', keySignature
    for c in phrase:        
        index += 1
        print c.romanNumeral, c.quarterLength
        total += c.quarterLength
        print 'total:', total
        if total > keySignature * measureInd:
            print [c.quarterLength for c in phrase], measureInd, keySignature
            raise ValueError("Chord isn't contained in a measure")
        if total == keySignature * measureInd:
            break
    return index

def create_new_phrase(phrases):
    p1 = random.choice(phrases)    
    keySignature = sum(c.quarterLength for c in p1) / 8.0
    p2 = random.choice(phrases)
    while sum(c.quarterLength for c in p2) / 8.0 != keySignature:
        p2 = random.choice(phrases)
    connection = random.randint(1, 7)
    index1 = getBreakPoint(p1, connection, keySignature)
    print index1
    index2 = getBreakPoint(p2, connection, keySignature)
    print index2
    result = p1[:index1] + p2[index2:]
    for num in result:
        num.key = key.Key('C')
    return result
    
def test_create_new_phrase():
    p1 = [roman.RomanNumeral('i'), roman.RomanNumeral('ii') \
        , roman.RomanNumeral('iii'),roman.RomanNumeral('iv')] * 2
    p2 = [roman.RomanNumeral('I'), roman.RomanNumeral('II') \
        , roman.RomanNumeral('III'),roman.RomanNumeral('IV')] * 2
    print create_new_phrase([p1, p2])

def step(phrase, cluster):
    print 'started step'
    best = (float('Inf'),None,None)
    for i in range(len(phrase)):
        rnbackup = phrase[i]
        for num in diantonicNumerals:
            nrn = roman.RomanNumeral(num)
            nrn.quarterLength = rnbackup.quarterLength
            nrn.key = rnbackup.key
            phrase[i] = nrn
            dist = distance_from_cluster(phrase, cluster)
            best = min(best,(dist, i, num))
        phrase[i] = rnbackup
    return best
    
def compose_harmony(phrases, clusters):
    target = random.choice(clusters)[1]
    current_harmony = create_new_phrase(phrases)
    phrases_in_cluster = [phrases[ind] for ind, cl_ind in clusters if cl_ind == target]
    for i in range(6):
        print [rn.romanNumeral for rn in current_harmony]
        dist, ind, num = step(current_harmony, phrases_in_cluster)
        nrn = roman.RomanNumeral(num)
        nrn.quarterLength = current_harmony[ind].quarterLength
        nrn.key = current_harmony[ind].key
        current_harmony[ind] = nrn
    return current_harmony, dist      
    
def distance_from_cluster(phrase, cluster):
    v1 = phraseHarmonicTensionVector(phrase)
    v2 = mean(array([phraseHarmonicTensionVector(p2) for p2 in cluster]), axis=0)
    
    return linalg.norm(v2-v1)

if __name__ == '__main__':        
    with open("..\\data\\pickled_phrases.obj") as f:
        phrases = pickle.load(f)
        
    with open('..\\data\\clusters.csv') as f:
        clusters = [tuple(map(int,line.split(","))) for line in f]    
    
    harmony = compose_harmony(phrases, clusters)[0]
    st = generateAccompenimentStream(harmony)
    print harmony