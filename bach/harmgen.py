import random

# this is the grammar used to derive harmonic phrases
rules = {
	'PC' : 'X X V I | X X III vi',
	'HC' : 'X X X D',
	'PlC' : 'X X SD T',
	'X' : 'T | SD | D',
	'T' : 'I | vi',
	'SD' : 'ii | IV',
	'D' : 'V | III'
}

rn_to_chord_CM = {
	'I' : 'C',
	'ii' : 'Dm',
	'III' : 'E',
	'IV' : 'F',
	'V' : 'G',
	'vi' : 'Am'
}

def generate_from(base):
    """
    this function randomly derives (with equal probability for each possible production rule) a phrase starting from the base parameter.
    returns the resulting phrase
    """
    result = base.split(' ')
    possible = [i for i,x in enumerate(result) if x in rules.keys()]
    while len(possible) > 0:
        j = random.choice(possible)
        result = result[:j] \
            + random.choice([x.strip().split(' ') for x in rules[result[j]].split('|')]) \
            + result[j+1:]
        possible = [i for i,x in enumerate(result) if x in rules.keys()]
    return result

if __name__ == '__main__':
    print [rn_to_chord_CM[rn] for rn in generate_from('HC PlC')]
    print [rn_to_chord_CM[rn] for rn in generate_from('HC PC')]