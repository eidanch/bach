# -*- coding: utf-8 -*-
"""
Created on Fri May 02 13:15:59 2014

@author: Eidan
"""

from sklearn import svm
from music21 import corpus
from kernels import streamToArray

def getMeasureClassifier():
    b1 = corpus.parse('bach/bwv108.6.xml')
    b2 = corpus.parse('bach/bwv66.6.xml')
    measures = [streamToArray(m.notes,0.5,8) for m in b1.parts[0].makeMeasures()] \
        + [streamToArray(m.notes,0.5,8) for m in b1.parts[1].makeMeasures()] \
        + [streamToArray(m.notes,0.5,8) for m in b1.parts[2].makeMeasures()] \
        + [streamToArray(m.notes,0.5,8) for m in b1.parts[3].makeMeasures()] 
    clf = svm.OneClassSVM(kernel='linear')
    clf.fit(measures)
    return clf
    
clf = getMeasureClassifier()
b1 = corpus.parse('bach/bwv108.6.xml')
measures1 = [streamToArray(m.notes,0.5,8) for m in b1.parts[0].makeMeasures()] \
        + [streamToArray(m.notes,0.5,8) for m in b1.parts[1].makeMeasures()] \
        + [streamToArray(m.notes,0.5,8) for m in b1.parts[2].makeMeasures()] \
        + [streamToArray(m.notes,0.5,8) for m in b1.parts[3].makeMeasures()] 
b2 = corpus.parse('bach/bwv66.6.xml')
measures2 = [streamToArray(m.notes,0.5,8) for m in b2.parts[0].makeMeasures()] \
        + [streamToArray(m.notes,0.5,8) for m in b2.parts[1].makeMeasures()] \
        + [streamToArray(m.notes,0.5,8) for m in b2.parts[2].makeMeasures()] \
        + [streamToArray(m.notes,0.5,8) for m in b2.parts[3].makeMeasures()] 
print clf.predict(measures1)
print clf.predict(measures2)