# -*- coding: utf-8 -*-
"""
Tools for handling data from the Peachnote database

Created on Thu May 08 12:46:08 2014

@author: Eidan
"""

from music21.stream import Stream
from music21.note import Note, Rest
from music21.chord import Chord
from music21.duration import Duration
import re

quant = 4.0 / 32

class PeachnoteDatagram(object):
    """
    Represents a single line in a peachnote file
    """
    def __init__(self, dtype, composer, ngram, year, count):
        self.dtype = dtype
        self.composer = composer
        self.ngram = ngram
        self.year = year
        self.count = count
        
    def __repr__(self):
        return '%s\t%s\t%s\t%s\t%s' % (self.dtype, self.composer, \
            self.ngram, self.year, self.count)


def parse_peachnote_datagram(line):
    """
    Parses a single line into a PeachnoteDatagram object
    """
    lst = line.split('\t')
    lst = (5 - len(lst))*['?'] + lst # Add unknown types
    pdg = PeachnoteDatagram(dtype=lst[0], composer=lst[1], ngram=lst[2], \
        year=lst[3], count=int(lst[4]))
    return pdg
    
def get_chord_from_entry(entry ,last_note):
    """
    parse the representation of a single chord to a Chord object
    """
    global quant
    duration_start_index = entry.find('(')
    if duration_start_index < 0:
        duration = 4
        offsets = [int(offset) for offset in entry.split('_')]
    else:
        duration = int(entry[duration_start_index + 1:-1])
        offsets = [int(offset) for offset in entry[:duration_start_index].split('_')]
    if not last_note:
        last_note = 0
    notes = [last_note + offsets[0]]
    for i in range(1, len(offsets)):
        notes.append(offsets[i] + notes[i - 1])
    c = Chord(notes)
    c.duration = Duration(duration * quant)
    return c, notes[-1]
    
def get_note_from_entry(entry ,last_note):
    """
    parse the representation of a single note to a Note object
    """
    global quant
    duration_start_index = entry.find('(')
    if duration_start_index < 0:
        duration = 4
        pitch = int(entry) if entry != 'r' else -1 
    else:
        duration = int(entry[duration_start_index + 1:-1])
        pitch = int(entry[:duration_start_index]) \
            if entry[:duration_start_index] != 'r' else -1
    if last_note:
        pitch += last_note
    n = Note(pitch) if pitch != -1 else Rest()
    n.duration = Duration(duration * quant)
    return n, pitch
    
def get_chord_from_abs_entry(entry):
    """
    parses an absolute pitch representation of a chord into a Chord object
    """
    global quant
    duration_start_index = entry.find('(')
    if duration_start_index < 0:
        duration = 4
        notes = [int(note) for note in entry.split('_')]
    else:
        duration = int(entry[duration_start_index + 1:-1])
        notes = [int(note) for note in entry[:duration_start_index].split('_')]
    c = Chord(notes)
    c.duration = Duration(duration * quant)
    return c
    
def get_abs_from_relative_chord(entry ,last_note):
    """
    takes a chord with relative representation and returns an absolute one
    """
    duration_start_index = entry.find('(')
    if duration_start_index < 0:
        duration = '4'
        offsets = [int(offset) for offset in entry.split('_')]
    else:
        duration = entry[duration_start_index + 1:-1]
        offsets = [int(offset) for offset in entry[:duration_start_index].split('_')]
    notes = [last_note + offsets[0]]
    for i in range(1, len(offsets)):
        notes.append(offsets[i] + notes[i - 1])
    result = '_'.join(str(note) for note in notes)
    result += '(' + duration + ')'
    return result, notes[-1]
        
def ngram_relative_to_absolute(ngram_string):
    """
    Turns an ngram into a music21 stream object
    """
    lst = ngram_string.split()
    last_note = 0
    result_list = []
    for entry in lst:
        # TODO: implement properly
        current_chord, last_note = get_abs_from_relative_chord(entry ,last_note)
        result_list.append(current_chord)
    return ' '.join(result_list)
      
    
def ngram_to_harmonic_stream(ngram_string):
    """
    Turns an ngram into a music21 stream of chord object
    """
    lst = ngram_string.split()
    last_note = None
    result_stream = Stream()
    for entry in lst:
        # TODO: implement properly
        current_chord, last_note = get_chord_from_entry(entry ,last_note)
        result_stream.append(current_chord)
    return result_stream
        
def ngram_to_melodic_stream(ngram_string):
    """
    Turns an ngram into a music21 stream of note object
    """
    lst = ngram_string.split()
    last_note = None
    result_stream = Stream()
    for entry in lst:
        # TODO: implement properly
        note, last_note = get_note_from_entry(entry ,last_note)
        result_stream.append(note)
    return result_stream
        

def datagram_iterator(fileptr, datagram_filter=None):
    """
    Iterates over the datagram objects in a peachnote file
    """
    for line in fileptr:
        pdg = parse_peachnote_datagram(line.rstrip())
        if datagram_filter is None or datagram_filter(pdg):
            yield pdg
            
class NgramCorpus(object):
    """
    Untested code
    """
    def __init__(self):
        self.ngram_dict = dict()
        
    def loadFromFile(self, infile, dfilter=None, progresslog=None):
        with open(infile, 'r') as fp:
            i = 0
            for pdg in datagram_iterator(fp, dfilter):
                i += 1
                if progresslog and i % progresslog == 0:
                    print 'progress:', i
                ngram = ngram_relative_to_absolute(pdg.ngram)
                if ngram not in self.ngram_dict:
                    self.ngram_dict[ngram] = 0
                self.ngram_dict[ngram] += pdg.count
        
    def saveToFile(self, outfile):
        with open(outfile, 'w') as fp:
            for ngram, count in self.ngram_dict.iteritems():
                pdg = PeachnoteDatagram(dtype='reduced', composer='?', \
                    ngram=ngram, year='?', count=count)
                fp.write(str(pdg) + '\n')
        
def create_quantum_dfilter(minlength):
    """
    Creates a datagram filter that filters out ngrams whose rhythmic
    content is more complex then the given quantum (durations or chords
    are shorter or evenly divided by the minlength value).
    """
    pattern = r'[0-9_\-]*\(([0-9]*)\)'
    prog = re.compile(pattern)
    def dfilter(pdg):
        chords = pdg.ngram.split(' ')
        for chord in chords:
            match_obj = prog.match(chord)
            if match_obj:
                dur = int(match_obj.groups()[0])
                if dur < minlength or dur % minlength != 0:
                    return False
        return True
    return dfilter
    
def reduce_peachnote_file(infile, outfile, dfilter=None, progresslog=None):
    """
    Retains only the ngrams and counts for datagrams passing the
    given filter. Data is summed for same-ngram datagrams.
    
    Tested manually
    """
    ngram_dict = dict()
    with open(infile, 'r') as fp:
        i = 0
        for pdg in datagram_iterator(fp, dfilter):
            i += 1
            if progresslog and i % progresslog == 0:
                print 'progress:', i
            if pdg.ngram not in ngram_dict:
                ngram_dict[pdg.ngram] = 0
            ngram_dict[pdg.ngram] += pdg.count
    with open(outfile, 'w') as fp:
        for ngram, count in ngram_dict.iteritems():
            pdg = PeachnoteDatagram(dtype='reduced', composer='?', \
                ngram=ngram, year='?', count=count)
            fp.write(str(pdg) + '\n')
    