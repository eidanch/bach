# -*- coding: utf-8 -*-
"""
Created on Tue Jun 24 20:16:28 2014

@author: Eidan
"""

import random

def argrand(f ,keys):
    """
    Returns a random index k from keys.
    The probability that k is returned is f(k)/(sum of f(k) for all k in keys)
    """
    psum = sum(map(f, keys))
    r = random.random()*psum
    curr = 0
    for k in keys:
        curr += f(k)
        if curr >= r:
            return k
    print "argrand Problem!"