# -*- coding: utf-8 -*-
"""
Created on Sat Jul 26 16:10:10 2014

@author: Hershenson
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Jul 22 17:38:20 2014

@author: Hershenson
"""
import random
import pickle
from music21 import roman, key
from theory import phraseHarmonicTensionVector
from numpy import array
from bachmeasuregenerator import generateAccompenimentStream
from sklearn.svm import OneClassSVM

diantonicNumerals = [
    'I',
    'ii',
    'iii',
    'IV',
    'V',
    'vi',
]

def getBreakPoint(phrase, measureInd, keySignature):
    total = 0
    index = 0
    print 'index:',measureInd, 'sig:', keySignature
    for c in phrase:        
        index += 1
        print c.romanNumeral, c.quarterLength
        total += c.quarterLength
        print 'total:', total
        if total > keySignature * measureInd:
            print [c.quarterLength for c in phrase], measureInd, keySignature
            raise ValueError("Chord isn't contained in a measure")
        if total == keySignature * measureInd:
            break
    return index

def create_new_phrase(phrases):
    p1 = random.choice(phrases)    
    keySignature = sum(c.quarterLength for c in p1) / 8.0
    p2 = random.choice(phrases)
    while sum(c.quarterLength for c in p2) / 8.0 != keySignature:
        p2 = random.choice(phrases)
    connection = random.randint(1, 7)
    index1 = getBreakPoint(p1, connection, keySignature)
    print index1
    index2 = getBreakPoint(p2, connection, keySignature)
    print index2
    result = p1[:index1] + p2[index2:]
    for num in result:
        num.key = key.Key('C')
    return result
    
def test_create_new_phrase():
    p1 = [roman.RomanNumeral('i'), roman.RomanNumeral('ii') \
        , roman.RomanNumeral('iii'),roman.RomanNumeral('iv')] * 2
    p2 = [roman.RomanNumeral('I'), roman.RomanNumeral('II') \
        , roman.RomanNumeral('III'),roman.RomanNumeral('IV')] * 2
    print create_new_phrase([p1, p2])

def step(phrase, clf):
    print 'started step'
    best = (float('-Inf'),None,None)
    for i in range(len(phrase)):
        rnbackup = phrase[i]
        for num in diantonicNumerals:
            nrn = roman.RomanNumeral(num)
            nrn.quarterLength = rnbackup.quarterLength
            nrn.key = rnbackup.key
            phrase[i] = nrn
            score = clf.decision_function(phraseHarmonicTensionVector(phrase))[0, 0]
            best = max(best,(score, i, num))
        phrase[i] = rnbackup
    return best
    
def compose_harmony(phrases):
    clf = OneClassSVM()
    clf.fit(array([phraseHarmonicTensionVector(phrase) for phrase in phrases]))
    current_harmony = create_new_phrase(phrases)
    print [rn.romanNumeral for rn in current_harmony]
    for i in range(6):
        score, ind, num = step(current_harmony, clf)
        nrn = roman.RomanNumeral(num)
        nrn.quarterLength = current_harmony[ind].quarterLength
        nrn.key = current_harmony[ind].key
        current_harmony[ind] = nrn
        print [rn.romanNumeral for rn in current_harmony], score
    return current_harmony, score      
    
if __name__ == '__main__':        
    with open("..\\data\\pickled_phrases.obj") as f:
        phrases = pickle.load(f)
        
    harmony = compose_harmony(phrases)[0]
    st = generateAccompenimentStream(harmony)