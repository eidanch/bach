# -*- coding: utf-8 -*-
"""
Created on Tue Jul 22 16:57:34 2014

@author: Hershenson
"""

import pickle
from sklearn.cluster import KMeans
from theory import phraseHarmonicTensionVector
from numpy import zeros, array



with open('..\\data\\pickled_phrases.obj') as f:
    phrases = pickle.load(f)



samples = zeros((len(phrases), 32))
for i in range(len(phrases)):
    samples[i] = array(phraseHarmonicTensionVector(phrases[i]))
estimator = KMeans(n_clusters=20)
clusters = estimator.fit_predict(samples)
print clusters
print estimator.cluster_centers_
for i in range(len(estimator.cluster_centers_)):
    print len([x for x in clusters if x == i])
with open('..\\data\\clusters.csv', 'w') as outfile:
    for i, c in enumerate(clusters):
        outfile.write("{}, {}\n".format(i, c))