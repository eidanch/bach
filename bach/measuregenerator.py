# -*- coding: utf-8 -*-
"""
Created on Wed May 07 17:53:23 2014

@author: Eidan
"""

from music21 import *
from peachnote import *
from theory import *
from measurelearner import *
from randomutils import argrand
from math import exp, log

class MeasureGenerator(object):
    def __init__(self, quantum):
        self.quantum = quantum
    
    def generate(self ,scale=None, previous=None, harmony=None, rhythm=None):
        current = 0.0
        currentstream = stream.Stream()
        while current < 1.0:
            nextnote = \
                get_next_note(previous,currentstream,scale,chord,rhythm)
            stream.append(nextnote)
            current += nextnote.quarterlength*self.quantum/4.0
        return stream
        
    def get_next_note(self):
        """
        That's the big question for this level of composition.
        I tend to believe that using peachnote would fit best here.
        
        Basically, use ngrams randomized completion, using previous and 
        current as data.
        Rhythm would be generated according to the ngrams or restricted to 
        a given rhythmic pattern.
        If a harmoney object was passed, we should use it to get more
        appropriate notes (chord notes) on the stronger beats (just tilt
        the randomization).
        A scale would represent a non compromising restriction of the set of
        notes we allow.
        """
        pass
    
    def generate_cocnrete(self ,scale, previous, harmony, rhythm):
        """
        Generates a measure given a scale, the previous measure, a chord
        and a rhythmic pattern
        """
        prev = previous[-1]
        currentstream = stream.Stream()
        for nt in rhythm.notes:
            next_note = self.get_next_note_concrete(scale,prev,harmony,nt)
            prev = next_note
            currentstream.append(next_note)
        return currentstream
        
    def get_next_note_concrete(self, scale,prev,harmony,nt):
        """
        Generates a note on a given scale, and chord, with a given rhythmic
        value and a previous note
        """
        p = self.get_pitch(scale, prev.pitch ,harmony, nt)
        n = note.Note(p, quarterLength=nt.duration.quarterLength)
        return n
        
    def get_pitch(self, scale, previous_pitch ,harmony, nt):
        """
        Gets a pitch for the new note using a random generation formula
        """
        pitch_weights = dict()
        pitches = scale.getPitches(previous_pitch.transpose('M-7'), \
            previous_pitch.transpose('M7'))
        for pitch in pitches:
            pitch_weights[pitch] = self.get_pitch_weight(pitch, previous_pitch, \
                nt, harmony, note.Note('C5'))
        result = argrand(lambda p: pitch_weights[p], pitches)
        print result, pitch_weights[result], max(pitches, key=lambda p: pitch_weights[p])
        return result
    
    def get_pitch_weight(self, pitch, previous_pitch, nt, harmony, center):
        """
        Returns a probabilistic weight for the given pitch, 
        in the given setting.
        What we want to take into account:
        Proportional to position significance * harmony significance #1
        Inverse Proportional to distance from previous pitch #2
        Inverse proportional to some pitch center #3
        """
        result = 1.
        dist = interval.notesToChromatic(previous_pitch, pitch).cents \
            / 100.
        cdist = interval.notesToChromatic(center, pitch).cents \
            / 100.
        #print pitch, previous_pitch, dist
        result = exp(-0.01*\
            (5*max(abs(dist - 2), abs(dist+2))**2*(-log(nt.beatStrength)) \
            + 10*abs(cdist)))
        #print cdist, result
        if pitch.pitchClass in harmony.pitchClasses:
            boost = 5
            if pitch.pitchClass == harmony.root().pitchClass:
                boost += 15
            if pitch.pitchClass == harmony.third.pitchClass:
                boost += 10
            if pitch.pitchClass == harmony.fifth.pitchClass:
                boost += 5
            boost *= 3
            result *= exp(0.01*boost*nt.beatStrength**3) #2 
        return result
   
def testGeneration():
    scl = scale.MajorScale('F')  
    mg = BachMeasureGenerator(8)
    bs = converter.parse("tinynotation: 4/4 c4 c8 c8 c4 b8 b8")
    bs2 = converter.parse("tinynotation: 4/4 c4 c4 c2")
    bs3 = converter.parse("tinynotation: 4/4 c4 c8 c8 c4 c4")
    bs4 = converter.parse("tinynotation: 4/4 c4 c4 c2")
    result = stream.Stream()
    accomp = stream.Stream()
    ps = bs
    for i in range(2):
        ct = chord.Chord(['f3','a3','c3'], quarterLength=4)
        csd = chord.Chord(['a3','c3','e3'], quarterLength=4)
        cd = chord.Chord(['b-3','d3','f3'], quarterLength=4)
        ct2 = chord.Chord(['c3','e3','g3'], quarterLength=4)
        accomp.append(ct)
        accomp.append(csd)
        accomp.append(cd)
        accomp.append(ct2)
        ns = mg.generate_cocnrete(scl, ps, ct, bs)
        for n in ns.notes:
            result.append(n)
        ps = ns
        ns = mg.generate_cocnrete(scl, ps, csd, bs2)
        for n in ns.notes:
            result.append(n)
        ps = ns
        ns = mg.generate_cocnrete(scl, ps, cd, bs3)
        for n in ns.notes:
            result.append(n)
        ps = ns
        ns = mg.generate_cocnrete(scl, ps, ct, bs4)
        for n in ns.notes:
            result.append(n)
        ps = ns
    return result + accomp
    
  
#r = test_generation()
#r.show()