# -*- coding: utf-8 -*-
"""
Created on Thu Jun 12 15:47:23 2014

@author: Eidan
"""

class Segment(object):
    """
    Represents a segment of a song
    """
    def __init__(self, name, key,duration, chords, keySignature):
        self.name = name
        self.key = key
        self.duration = duration
        self.chords = chords
        self.keySignature = keySignature
    def __repr__(self):
        return "Segment %s in the key of %s and duration of %d with key signature %d/4" \
            % (self.name, self.key, self.duration, self.keySignature)
    
def parseSong(segFile, keyFile, chordFile, beatFile):
    """
    Gets the names of the files containing information about a song
    Returns a list of segments in the song
    """
    segments = list()
    with open(segFile) as fp:
        for line in fp:
            lst = line.strip().split('\t')
            segments.append((lst[3],float(lst[0]), float(lst[1])))
    keys = list()
    with open(keyFile) as fp:
        for line in fp:
            lst = line.strip().split('\t')
            keys.append((lst[-1],float(lst[0]), float(lst[1])))
    chords = list()
    with open(chordFile) as fp:
        for line in fp:
            lst = line.strip().split(' ')
            chords.append((lst[2],float(lst[0]), float(lst[1])))
    beats = list()
    with open(beatFile) as fp:
        for line in fp:
            lst = line.strip().split()
            beats.append((int(lst[-1]), float(lst[0])))
    return segments, keys, chords, beats

def averageBeatLength(beats):
    """
    calculates the average length of a beat in the song
    """
    lengthsum = 0.0
    for i in range(len(beats) - 1):
        lengthsum += beats[i+1][1] - beats[i][1]
    return lengthsum/(len(beats) - 1)

def discetizeSongDataSingleList(lst, beats, roundvalue=0.3):
    """
    gets a list of data of timed events in a song and a timed list of beats 
    and returns the data coupled with its duration in beats
    """
    dlst = list()
    j = 0
    count = 0
    while lst[j][2] < beats[0][1]:
        j += 1
    i = 0
    while i < len(beats):
        if abs(lst[j][2] - beats[i][1]) < roundvalue:
            if count > 0:
                dlst.append((lst[j][0], count))
            count = 0
            j += 1
        else:
            count += 1
            i += 1
    if count > 0:
        dlst.append((lst[j][0], count))
    return dlst

def discretizeSongData(segments, keys, chords, beats, roundvalue=0.3):
    """
    Gets lists of timed data of segments, keys and beats in a song and a timed list of beats 
    and returns the data coupled with its duration in beats
    """
    dsegments = discetizeSongDataSingleList(segments, beats, roundvalue)
    dkeys = discetizeSongDataSingleList(keys, beats, roundvalue)
    dchords = discetizeSongDataSingleList(chords, beats, roundvalue)
    return dsegments, dkeys, dchords
    
def parseAndDiscretizeSongData(segFile, keyFile, chordFile, beatFile):
    """
    Parses a song whose data is contained in the specified files, and 
    discretizes the data to a structure specifying the duration in beats of each item
    """
    segments, keys, chords, beats = \
        parseSong(segFile, keyFile, chordFile, beatFile)
    roundvalue = averageBeatLength(beats)/(2.1)
    keySignature = max(beat[0] for beat in beats)
    return discretizeSongData(segments, keys, chords, beats, roundvalue), keySignature

def createSegments(segments, keys, chords, keySignature):
    """
    Gets lists describing a song's segments, keys and chords, along with its time signature, 
    and constructs a list of Segment objects describing the song   
    """    
    if keySignature > 4:
        raise ValueError('Bad key signature')
    keyBeatsLeft = keys[0][1]
    currentKeyIndex = 0
    currentChordIndex = 0
    result = list()    
    for segmentName, segmentLength in segments:
        chordBeatsLeft = segmentLength
        segmentChords = list()
        while chordBeatsLeft > 0:
            chordName, chordLength = chords[currentChordIndex]
            chordBeatsLeft -= chordLength
            segmentChords.append((chordName, chordLength))
            currentChordIndex += 1
        if chordBeatsLeft < 0:
            raise ValueError('Unaligned segments')

        result.append(Segment(segmentName, keys[currentKeyIndex][0], \
            segmentLength, segmentChords, keySignature))
        keyBeatsLeft -= segmentLength
        if keyBeatsLeft <= 0:
            currentKeyIndex += 1
            if currentKeyIndex < len(keys):
                keyBeatsLeft = keys[currentKeyIndex][1]
    return result
    
def getSegments(album, song):
    """
    returns a list of segments in a beatles song with the corresponding album and song names
    """
    beatFile = '..\\data\\The Beatles Annotations\\beat\\The Beatles\\%s\\%s.txt' % (album, song)
    chordFile = '..\\data\\The Beatles Annotations\\chordlab\\The Beatles\\%s\\%s.lab' % (album, song)
    keyFile = '..\\data\\The Beatles Annotations\\keylab\\The Beatles\\%s\\%s.lab' % (album, song)
    segFile = '..\\data\\The Beatles Annotations\\seglab\\The Beatles\\%s\\%s.lab' % (album, song)
    (dsegments, dkeys, dchords), keySignature = parseAndDiscretizeSongData(segFile, keyFile, chordFile, beatFile)
    segments = createSegments(dsegments, dkeys, dchords, keySignature) 
    return segments
    
    
def test():
    getSegments('05_-_Help!', '13_-_Yesterday')
    getSegments("11_-_Abbey_Road", "01_-_Come_Together")
    getSegments("08_-_Sgt._Pepper's_Lonely_Hearts_Club_Band", \
        "03_-_Lucy_In_The_Sky_With_Diamonds")
    getSegments("08_-_Sgt._Pepper's_Lonely_Hearts_Club_Band", \
        "01_-_Sgt._Pepper's_Lonely_Hearts_Club_Band")
    getSegments('12_-_Let_It_Be', '06_-_Let_It_Be')
    
    