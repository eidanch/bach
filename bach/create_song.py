# -*- coding: utf-8 -*-
"""
Created on Mon Jul 28 12:11:26 2014

@author: Hershenson
"""

import pickle
from sklearn.cluster import KMeans
from numpy import array
from music21 import scale, stream, tempo, dynamics, key
import random
import copy

from theory import phraseHarmonicTensionVector
from bachmeasuregenerator import generateAccompenimentStream, generateMelodicStream
from cluster_harm import generate_good_phrase, strings_to_numerals


relative_dominant = {
    'C' : 'G',
    'D' : 'A',
    'B-' : 'F',
    'F' : 'C',
    'G' : 'D',
    'A' : 'E'
}

def get_clusterer():
    """
    returns an sklearn clusterer based on the phrases corpus vectorized to the harmonic tension space
    """
    with open("..\\data\\pickled_phrases.obj") as f:
        phrases = pickle.load(f)
    samples = array([phraseHarmonicTensionVector(phrase) for phrase in phrases])
    clusterer = KMeans(n_clusters=10)
    clusterer.fit(samples)
    return clusterer
    
def create_ternary_song():
    """
    This function generates a musical piece of the ternary form (ABA).
    returns a music21 Stream object.
    """
    clusterer = get_clusterer()
    A = generate_good_phrase(clusterer, random.choice(['HC PC', 'HC PlC'])) + \
        generate_good_phrase(clusterer, random.choice(['HC PC', 'HC PlC']))
        
    B = generate_good_phrase(clusterer, random.choice(['HC PC', 'HC PlC'])) + \
        generate_good_phrase(clusterer, random.choice(['HC PC', 'HC PlC']))
    tonic_scale = random.choice(relative_dominant.keys())
    dominant_scale = relative_dominant[tonic_scale]
    aNum = strings_to_numerals(A, tonic_scale)
    stA = generateMelodicStream(scale.MajorScale(tonic_scale), aNum, 'Soprano', end_of_piece=True) + \
            generateAccompenimentStream(aNum)
            
    stAP = copy.deepcopy(stA)
            
    bNum = strings_to_numerals(B, dominant_scale)
    stB = generateMelodicStream(scale.MajorScale(dominant_scale), bNum, 'Soprano') + \
            generateAccompenimentStream(bNum)
            
    song = stream.Score()
    song.append(stA)
    song.append(stB)
    song.append(stAP)
    song = song.flat
    song.insert(0.0, key.Key(tonic_scale))
    song.insert(0.0, tempo.MetronomeMark('presto'))
    song.insert(0.0, dynamics.Dynamic('ff'))
    return song.flat
    
def create_rondo_song():
    """
    This function generates a musical piece of the rondo form (ABACA).
    returns a music21 Stream object.
    """
    clusterer = get_clusterer()
    tonic_scale = random.choice(relative_dominant.keys())
    dominant_scale = relative_dominant[tonic_scale]
    A = generate_good_phrase(clusterer, random.choice(['HC PC', 'HC PlC']))
        
    B = generate_good_phrase(clusterer, random.choice(['HC PC', 'HC PlC']))
    
    C = generate_good_phrase(clusterer, random.choice(['HC PC', 'HC PlC']))
        
    aNum = strings_to_numerals(A, tonic_scale)
    stA = generateMelodicStream(scale.MajorScale(tonic_scale), aNum, 'Soprano', end_of_piece=True) + \
            generateAccompenimentStream(aNum)
            
    stAP = copy.deepcopy(stA)
    stAPP = copy.deepcopy(stA)
            
    bNum = strings_to_numerals(B, dominant_scale)
    stB = generateMelodicStream(scale.MajorScale(dominant_scale), bNum, 'Soprano') + \
            generateAccompenimentStream(bNum)
            
    cNum = strings_to_numerals(C, dominant_scale)
    stC = generateMelodicStream(scale.MajorScale(dominant_scale), cNum, 'Soprano') + \
            generateAccompenimentStream(cNum)
            
    song = stream.Stream()
    stA.insert(0, tempo.MetronomeMark('presto'))
    stA.insert(0, dynamics.Dynamic('ff'))
    song.append(stA)
    song.append(stB)
    song.append(stAP)
    song.append(stC)
    song.append(stAPP)
    return song.flat