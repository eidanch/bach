"""
kernels.py

Implements several musical kernel related functions
"""

from music21 import *
import numpy as np

def parallel_stream_scan(s1,s2):
    """
    Iterates over the two streams in parallel and yields
    points on which at least one of the streams starts a new note.
    """
    i1,i2 = 0,0
    t1,t2 = 0,0
    while i1 < len(s1) and i2 < len(s2):
        n1,n2 = s1[i1], s2[i2]
        dur1, dur2 = n1.quarterLength, n2.quarterLength
        if dur1 - t1 == dur2 - t2:
            dur = dur1 - t1
            i1 += 1
            i2 += 1
        elif dur1 - t1 < dur2 - t2:
            dur = dur1 - t1
            i1 += 1
        else:
            dur = dur2 - t2
            i2 += 1
        t1 += dur
        if t1 == dur1:
            t1 = 0
        t2 += dur
        if t2 == dur2:
            t2 = 0
        yield (n1, n2, dur)

def pitch_kernel(s1, s2):
    """
    Calculates the kernel pitch as described in the document
    """
    result = 0
    for p1, p2, dur in parallel_stream_scan(s1,s2):
        print n1,n2,dur
        result += n1.midi*n2.midi*dur/4.0
    return result

def rythem_kernel(s1, s2):
    """
    I don't think this one works... maybe fix it according to the document
    """
    result = 0
    for n1, n2, dur in parallel_stream_scan(s1,s2):
        if n1.quarterLength == dur and n2.quarterLength == dur:
            result += 1
    return result

def dist(s1,s2, kern):
    """
    Just implements distance between two streams according to a given kernel
    """
    return pitch_kernel(s1,s1) + pitch_kernel(s2,s2) - 2*pitch_kernel(s1,s2)

def getMidiAt(stream, at):
    """
    Returns the midi value of the last note played before a given time
    """
    x = stream.getElementAtOrBefore(at)
    if x:
        return x.midi
    return -1

def streamToArray(stream, quantum, length):
    """
    Samples the pitches at discrete time using a given time quantum and a
    given target length
    
    Standard inner product on these vectors implements the pitch kernel
    """
    return np.array([getMidiAt(stream, quantum*k)\
        for k in range(length)])
            
def weight(length):
    """
    This function creates a weighting for the inner product that amplifies
    the value of strong beats interatctions.
    
    It is known that the first beat is stronger and more meaningful
    then the 4th beat and that the 2nd eigth-note is likely to be 
    less significant then the 5th eigth-note
    Assuming we are aligned with a measure and 4/4 meter    
    
    >>> weight(8)
    array([ 8.,  1.,  2.,  1.,  4.,  1.,  2.,  1.])
    """
    w = np.array([.0]*length)
    i = 2
    while i <= length:
        for j in xrange(0,length,i):
            w[j] += 1
        i *= 2
    return np.power(2,w)

"""
Just some examples I used to check how the kernels behaves,
Not very informative but it helped check basic stuff works,
"""
s1 = converter.parse("tinynotation: c4 e g c")
s2 = converter.parse("tinynotation: c4 f a c")
s2t = converter.parse("tinynotation: c4 f8 f8 a4 c")
s3 = converter.parse("tinynotation: e4 g# b e")
