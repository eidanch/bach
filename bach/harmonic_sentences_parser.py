# -*- coding: utf-8 -*-
"""
Created on Mon Jul 14 17:27:14 2014

@author: Hershenson
"""
import os.path
from songparse import getSegments
from music21 import harmony, roman, chord, key
import pickle

def transformChordName(name):
    transformed = name[0]
    if len(name) == 1:
        return transformed
    if name[1] == '#':
        transformed += '#'
    if name[1] == 'b':
        transformed += '-'
    if ':' in name and 'min' in name.split(':')[1]:
        transformed += 'm'
    return transformed
        
            
def get_phrases(segment):
    phrase_len_in_measures = 8
    current_len = 0
    curr_phrase = []
    phrases = []
    if segment.keySignature == 4:
        for c in segment.chords:
            current_len += c[1]
            if c[0] == 'N':
                raise ValueError('Phrases contains rest')
            cs = harmony.ChordSymbol(transformChordName(c[0]))
            rn = roman.romanNumeralFromChord(chord.Chord(cs), key.Key(segment.key[0]))        
            rn.quarterLength = c[1]
            curr_phrase.append(rn)
            if current_len > phrase_len_in_measures * segment.keySignature:
                raise ValueError('Bad phrase length')
            elif current_len == phrase_len_in_measures * segment.keySignature:
                phrases.append(curr_phrase)
                current_len = 0
                curr_phrase = []
    return phrases

error_count = 0
phrases = []
song_list_file = os.path.join('..', 'data', 'beatles_song_list.csv')
with open(song_list_file) as song_list:
    for j, line in enumerate(song_list):
        segments = []
        try:
           segments = getSegments(*line.strip().split())
        except ValueError:
            error_count+=1
        except:
            print '_________'
        for i, segment in enumerate(segments):
            print 'finished segment {}'.format(i)
            try:
                phrases += get_phrases(segment)
            except ValueError:
                error_count+=1
        print "finished song {}".format(j)
print error_count
print len(phrases)
print phrases

with open('..\\data\\pickled_phrases.obj', 'w') as outfile:
    pickle.dump(phrases, outfile)
print 'success'
