# -*- coding: utf-8 -*-
"""
Created on Sat Jul 26 17:12:32 2014

@author: Hershenson
"""

from harmgen import generate_from
from music21 import roman, scale
from bachmeasuregenerator import generateAccompenimentStream, generateMelodicStream
from theory import phraseHarmonicTensionVector
from numpy import array
from sklearn.svm import OneClassSVM
import pickle


def strings_to_numerals(phrase):
    numerals = [roman.RomanNumeral(x) for x in phrase]
    for numeral in numerals:
        numeral.quarterLength = 4.0
    return numerals

def generate_good_phrase(clf, base):
    best = (float('-Inf'), None)
    for i in range(32):
        phrase = generate_from(base)
        numerals = strings_to_numerals(phrase)
        current = clf.decision_function(phraseHarmonicTensionVector(numerals))[0, 0]
        best = max(best, (current, phrase))
    return best[1]

if __name__ == '__main__':        
    with open("..\\data\\pickled_phrases.obj") as f:
        phrases = pickle.load(f)
    
    with open('..\\data\\clusters.csv') as f:
        clusters = [tuple(map(int,line.split(","))) for line in f]    
    
    clf = OneClassSVM()
    samples = array([phraseHarmonicTensionVector(phrase) for phrase in phrases])
    clf.fit(samples)
    p1 = generate_good_phrase(clf, 'HC PlC')
    p2 = generate_good_phrase(clf, 'HC PC')
    best_numerals = strings_to_numerals(p1 + p2)
    st = generateMelodicStream(scale.MajorScale('C'), best_numerals, 'Soprano') + \
        generateAccompenimentStream(best_numerals)
    
    st.show()