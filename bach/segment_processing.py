import os.path
from songparse import getSegments
from music21 import harmony, roman, chord, key
import pickle

def transformChordName(name):
    """
    transforms the name of a chord from the format used in our corpus to that needed by music21
    """
    transformed = name[0]
    if len(name) == 1:
        return transformed
    if name[1] == '#':
        transformed += '#'
    if name[1] == 'b':
        transformed += '-'
    if ':' in name and 'min' in name.split(':')[1]:
        transformed += 'm'
    return transformed
        
            
def get_phrases(segment):
    """
    return a list of harmonic phrases in the segment
    """
    phrase_len_in_measures = 8
    current_len = 0
    curr_phrase = []
    phrases = []
    if segment.keySignature == 4:
        for c in segment.chords:
            current_len += c[1]
            if c[0] == 'N':
                raise ValueError('Phrases contains rest')
            cs = harmony.ChordSymbol(transformChordName(c[0]))
            rn = roman.romanNumeralFromChord(chord.Chord(cs), key.Key(segment.key[0]))              
            curr_phrase.append((rn.romanNumeral, c[1]))
            if current_len > phrase_len_in_measures * segment.keySignature:
                raise ValueError('Bad phrase length')
            elif current_len == phrase_len_in_measures * segment.keySignature:
                phrases.append(curr_phrase)
                current_len = 0
                curr_phrase = []
        return phrases
    raise ValueError('Bad time signature')

if __name__ == '__main__':
    """
    gets a list of all harmonic phrases in the corpus of beatles songs
    """ 
    error_count = 0
    phrases = []
    song_list_file = os.path.join('..', 'data', 'beatles_song_list.csv')
    with open(song_list_file) as song_list:
        for j, line in enumerate(song_list):
            segments = []
            try:
               segments = getSegments(*line.strip().split())
            except ValueError:
                error_count+=1
            except:
                print '_________'
            for i, segment in enumerate(segments):
                print 'finished segment {}'.format(i)
                try:
                    seg_phrases = get_phrases(segment)
                except ValueError:
                    error_count += 1
            print "finished song {}".format(j)
    print error_count
    print len(phrases)
    print phrases