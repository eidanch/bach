from music21 import roman, scale, key
from numpy import array, linalg
from sklearn.cluster import KMeans
import pickle

from harmgen import generate_from
from bachmeasuregenerator import generateAccompenimentStream, generateMelodicStream
from theory import phraseHarmonicTensionVector

def strings_to_numerals(phrase, key_name='C'):
    """
    receives a list of string representing numeral representation of chords and an optional key, returns matching music21 numeral objects in that key
    """
    numerals = [roman.RomanNumeral(x) for x in phrase]
    for numeral in numerals:
        numeral.quarterLength = 4.0
        numeral.key = key.Key(key_name)
    return numerals

def quality(clusterer, numerals):
    """
    estimates the quality of a musical phrase by the minimal distance to the cesnter of a cluster of phrases in the corpus 
    """
    return min(linalg.norm(phraseHarmonicTensionVector(numerals) - center) for center in clusterer.cluster_centers_)

def generate_good_phrase(clusterer, base, N=32):
    """
    generates N different phrases derived from the base and returns the best one as estimated using the clusterer
    """
    best = (float('Inf'), None)
    s = 0.0
    for i in range(N):
        phrase = generate_from(base)
        numerals = strings_to_numerals(phrase)
        current = quality(clusterer, numerals)
        s += current
        best = min(best, (current, phrase))
    return best[1]

if __name__ == '__main__':
    """
    generates a simple melody using generate_good_phrase and diplays it 
    """
    with open("..\\data\\pickled_phrases.obj") as f:
        phrases = pickle.load(f)
    samples = array([phraseHarmonicTensionVector(phrase) for phrase in phrases])
    clusterer = KMeans(n_clusters=10)
    clusters = clusterer.fit(samples)
    p1 = generate_good_phrase(clusterer, 'HC PlC')
    p2 = generate_good_phrase(clusterer, 'HC PC')
    best_numerals = strings_to_numerals(p1 + p2)
    st = generateMelodicStream(scale.MajorScale('C'), best_numerals, 'Soprano') + \
        generateAccompenimentStream(best_numerals)
    st.show()