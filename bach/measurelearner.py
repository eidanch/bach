# -*- coding: utf-8 -*-
"""
Created on Sat Jun 07 11:39:26 2014

@author: Eidan
"""

from music21 import *
from theory import *
from peachnote import *

def noteToNgramString(nt, previous_midi):
    """
    Return a peachnote ngram format string representing a note 
    """
    if nt.isRest:
        return "r(%d)" % (nt.duration.quarterLength*8)
    return "%d(%d)" % \
        (nt.pitch.midi - previous_midi, nt.duration.quarterLength*8)

def streamToNgramString(strm):
    """
    Return a peachnote ngram format string representing a stream of notes
    """
    result = ""
    prev_midi = 0
    for nt in strm.notesAndRests:
        result += noteToNgramString(nt, prev_midi) + ' '
        if nt.isRest:
            prev_midi = 0
        else:
            prev_midi = nt.pitch.midi
    return result.strip()
    
def createBachMeasureCorpus(outfp):
    """
    Creates a measure corpus file from the bach chorales database
    """
    for workName in corpus.getBachChorales():
        try:
            work = converter.parse(workName)
            print work
            for part in work.parts:
                for m in part:
                    if m.isStream:
                        outfp.write(streamToNgramString(m) + '\n')
        except:
            continue
        

def createBachMeasureCorpusByParts():
    """
    Creates a measure corpus file from the bach chorales database
    """
    filename_pattern = '..\\data\\bach_measures_%s.csv'
    part_names = ['Soprano', 'Alto', 'Tenor', 'Bass']
    fps = [open(filename_pattern % part_name, 'w') \
        for part_name in part_names]
    for workindex, workName in enumerate(corpus.getBachChorales()):
        try:
            work = converter.parse(workName)
            print workindex, work
            # timesig = work.getTimeSignatures()[0].ratioString
            for i, part_name in enumerate(part_names):
                #scale = work.parts[part_name] \
                #    .getKeySignatures()[0].getScale()
                #transposer = interval.Interval(scale.getTonic(), defaultTonic)
                #work.parts[part_name].transpose(transposer, inPlace=True)
                for m in work.parts[part_name]:
                    if m.isStream:
                        fps[i].write('%s\n' \
                            % (streamToNgramString(m)))
                        #fps[i].write('%s\t%s\t%s\n' \
                        #    % (scale.name, timesig, streamToNgramString(m)))
        except:
            print 'Exception! at work ', workindex
            continue
    for fp in fps:
        fp.close()

def streamToRhythemTuple(st):
    lst = []
    for nt in strm.notesAndRests:
        lst.append(int(8*nt.duration.quarterLength))
    return tuple(lst)
    
class RhythemCorpus(object):
    """
    Represents a corpus of weighted rhythmic patterns
    """
    def __init__(self):
        self.dictionary = dict()
    def addStream(self, strm):
        tpl = streamToRhythemTuple(strm)
        if tpl in self.dictionary:
            self.dictionary[tpl] += 1
        else:
            self.dictionary[tpl] = 1
            
def createBachRhythemCorpus(outfilename):
    """
    Create a RhythemCorpus from bach's chorales database
    """
    rc = RhythemCorpus()
    for workName in corpus.getBachChorales():
        try:
            work = converter.parse(workName)
        except:
            continue  
        print work
        for part in work.parts:
            for m in part:
                if m.isStream:
                    rc.addStream(m)
    with open(outfilename, 'w') as fp:
        for key in rc.dictionary.keys():
            fp.write(' '.join(map(str,key)) + '\t'\
                + str(rc.dictionary[key]) + '\n')
    return rc

def addTimeSignatureToCorpus(infile, outfile):
    """
    Tags an ngram corpus with time signature data
    """
    i = 0
    with open(infile) as fp:
        with open(outfile, 'w') as fpout:
            for line in fp:
                try:
                    st = ngram_to_melodic_stream(line)
                    ts = meter.bestTimeSignature(st)
                    fpout.write(ts.ratioString + '\t' + line)
                except meter.MeterException:
                    print 'exception at', i
                if i % 1000 == 0:
                    print i
                i += 1
            
def createMeasureCorpus(filename, ts):
    """
    Returns a list of ngrams imported from a corpus filtered
    by time signature
    """
    result = list()
    with open(filename) as fp:
        for line in fp:
            currts,ngram = tuple(line.split('\t'))
            if currts == ts:
                result.append(ngram)
    return result
    

def ngram_to_melodic_measure(ngram):
    measure = stream.Measure(ngram_to_melodic_stream(ngram))
    measure.timeSignature = meter.TimeSignature('4/4')
    return measure

def getBiasedWeightCorpus(ngramList, scl, chord):
    """
    Returns a dictionary of ngrams to harmonic conformance value, filtered
    by scale
    """
    result = dict()
    hc_lower_bound = 0
    for ngram in ngramList:
        st = ngram_to_melodic_measure(ngram)
        hc = harmonicConformance(st, chord, scl)
        if (hc > hc_lower_bound):    
            result[ngram] = hc
    return result


#c = converter.parse(corpus.getBachChorales()[0])
#createBachRhythemCorpus()
#with open('bachout.csv', 'w') as fp:
#     createBachMeasureCorpus(fp)

